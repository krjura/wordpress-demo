package org.krjura.wordpress.spring.restdocs.demo.services;

import org.krjura.wordpress.spring.restdocs.demo.model.Product;
import org.krjura.wordpress.spring.restdocs.demo.utils.PageInfo;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    void addProduct(String group, Product product);

    List<Product> findProduct(String group, String searchTerm, PageInfo pageInfo);

    Optional<Product> findProductById(String group, String productId);
}
