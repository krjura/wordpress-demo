package org.krjura.wordpress.spring.restdocs.demo.services;

import org.krjura.wordpress.spring.restdocs.demo.pojo.ImmutableCart;

import java.util.Optional;

public interface CartService {

    ImmutableCart createCart();

    ImmutableCart addProductToCart(String cartId, String group, String productId);

    Optional<ImmutableCart> findCart(String cartId);

    boolean removeProductFromCart(String cartId, String productId);
}
