package org.krjura.wordpress.spring.restdocs.demo.utils;

public class PageInfo {

    private short page;

    private short maxItems;

    public short getPage() {
        return page;
    }

    public void setPage(short page) {
        this.page = page;
    }

    public short getMaxItems() {
        return maxItems;
    }

    public void setMaxItems(short maxItems) {
        this.maxItems = maxItems;
    }

    public int getOffset() {
        return page * maxItems;
    }
}
