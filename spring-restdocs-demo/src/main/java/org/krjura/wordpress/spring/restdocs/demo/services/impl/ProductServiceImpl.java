package org.krjura.wordpress.spring.restdocs.demo.services.impl;

import org.krjura.wordpress.spring.restdocs.demo.model.Product;
import org.krjura.wordpress.spring.restdocs.demo.services.ProductService;
import org.krjura.wordpress.spring.restdocs.demo.utils.PageInfo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private Map<String, List<Product>> productMap;

    public ProductServiceImpl() {
        this.productMap = new HashMap<>();
    }

    public void addProduct(String group, Product product) {
        List<Product> products = this.productMap.computeIfAbsent(group, key -> new ArrayList<>());
        products.add(product);
    }

    @Override
    public List<Product> findProduct(String group, String searchTerm, PageInfo pageInfo) {
        String searchTermInLc = searchTerm.toLowerCase();

        return this
                .productMap
                .getOrDefault(group, Collections.emptyList())
                .stream()
                .filter(product -> product.getNameForSearch().contains(searchTermInLc))
                .skip(pageInfo.getOffset())
                .limit(pageInfo.getMaxItems())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Product> findProductById(String group, String productId) {
        return this
                .productMap
                .getOrDefault(group, Collections.emptyList())
                .stream()
                .filter(product -> product.getId().equals(productId))
                .findFirst();
    }
}
