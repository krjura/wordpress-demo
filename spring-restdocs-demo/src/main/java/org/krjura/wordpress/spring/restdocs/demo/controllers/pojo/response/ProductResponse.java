package org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response;

import org.krjura.wordpress.spring.restdocs.demo.model.Product;

public class ProductResponse {

    private String id;

    private String group;

    private String name;

    private String description;

    private float price;

    private String soldBy;

    private short rating;

    public ProductResponse(Product product) {
        this.id = product.getId();
        this.group = product.getGroup();
        this.name = product.getName();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.soldBy = product.getSoldBy();
        this.rating = product.getRating();
    }

    public String getId() {
        return id;
    }

    public String getGroup() {
        return group;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getPrice() {
        return price;
    }

    public String getSoldBy() {
        return soldBy;
    }

    public short getRating() {
        return rating;
    }
}