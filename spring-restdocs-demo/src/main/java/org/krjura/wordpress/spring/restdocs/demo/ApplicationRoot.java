package org.krjura.wordpress.spring.restdocs.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class ApplicationRoot {

    public static void main(String[] args) {
        ApplicationRoot application = new ApplicationRoot();
        application.runSpring(args);
    }

    private void runSpring(String[] args) {
        SpringApplication springApplication = new SpringApplication(ApplicationRoot.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }
}