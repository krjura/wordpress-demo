package org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response;

import org.krjura.wordpress.spring.restdocs.demo.model.Product;

import java.util.List;
import java.util.stream.Collectors;

public class ProductResponses {

    private List<ProductResponse> products;

    public ProductResponses(List<Product> products) {
        this.products = products.stream().map(ProductResponse::new).collect(Collectors.toList());
    }

    public List<ProductResponse> getProducts() {
        return products;
    }
}