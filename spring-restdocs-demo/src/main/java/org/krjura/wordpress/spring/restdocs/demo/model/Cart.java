package org.krjura.wordpress.spring.restdocs.demo.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Cart {

    private String id;

    private List<Product> products;

    public Cart(String id) {
        this.id = id;
        this.products = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }

    public boolean removeProduct(String productId) {
        return products.removeIf(product -> product.getId().equals(productId));
    }
}
