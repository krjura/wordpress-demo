package org.krjura.wordpress.spring.restdocs.demo.pojo;

import org.krjura.wordpress.spring.restdocs.demo.model.Cart;
import org.krjura.wordpress.spring.restdocs.demo.model.Product;

import java.util.List;
import java.util.Objects;

public class ImmutableCart {

    private final String id;

    private final List<Product> products;

    public ImmutableCart(String id, List<Product> products) {
        Objects.requireNonNull(id);

        this.id = id;
        this.products = products;
    }

    public static ImmutableCart of(Cart cart) {
        // product is already immutable
        return new ImmutableCart(cart.getId(), cart.getProducts());
    }

    public String getId() {
        return id;
    }

    public List<Product> getProducts() {
        return products;
    }
}
