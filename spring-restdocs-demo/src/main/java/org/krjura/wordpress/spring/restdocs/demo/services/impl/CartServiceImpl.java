package org.krjura.wordpress.spring.restdocs.demo.services.impl;

import org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response.CartInfo;
import org.krjura.wordpress.spring.restdocs.demo.ex.CartException;
import org.krjura.wordpress.spring.restdocs.demo.model.Cart;
import org.krjura.wordpress.spring.restdocs.demo.model.Product;
import org.krjura.wordpress.spring.restdocs.demo.pojo.ImmutableCart;
import org.krjura.wordpress.spring.restdocs.demo.services.CartService;
import org.krjura.wordpress.spring.restdocs.demo.services.ProductService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class CartServiceImpl implements CartService {

    private Map<String, Cart> cartMap;

    private final ProductService productService;

    public CartServiceImpl(ProductService productService) {
        Objects.requireNonNull(productService);

        this.productService = productService;
        this.cartMap = new HashMap<>();
    }

    @Override
    public ImmutableCart createCart() {
        String id = UUID.randomUUID().toString();

        cartMap.put(id, new Cart(id));

        return new ImmutableCart(id, Collections.emptyList());
    }

    @Override
    public ImmutableCart addProductToCart(String cartId, String group, String productId) {
        Product product = this
                .productService
                .findProductById(group, productId)
                .orElseThrow(() -> new CartException("Cannot find product with id of " + productId + " in group " + group));

        if(!this.cartMap.containsKey(cartId)) {
            throw new CartException("cannot find cart with id of " + cartId);
        }

        Cart cart = this.cartMap.get(cartId);
        cart.addProduct(product);

        return ImmutableCart.of(cart);
    }

    @Override
    public Optional<ImmutableCart> findCart(String cartId) {
        return this.cartMap.containsKey(cartId)
                ? Optional.of(ImmutableCart.of(this.cartMap.get(cartId)))
                : Optional.empty();
    }

    @Override
    public boolean removeProductFromCart(String cartId, String productId) {
        return this.cartMap.containsKey(cartId) && this.cartMap.get(cartId).removeProduct(productId);
    }
}