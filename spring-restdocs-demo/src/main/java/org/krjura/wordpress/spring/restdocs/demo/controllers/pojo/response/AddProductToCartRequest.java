package org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response;

public class AddProductToCartRequest {

    private String group;

    private String productId;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    // fluent

    public AddProductToCartRequest group(final String group) {
        setGroup(group);
        return this;
    }

    public AddProductToCartRequest productId(final String productId) {
        setProductId(productId);
        return this;
    }
}
