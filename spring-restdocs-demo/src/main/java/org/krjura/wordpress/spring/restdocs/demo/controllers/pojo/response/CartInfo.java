package org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response;

import org.krjura.wordpress.spring.restdocs.demo.pojo.ImmutableCart;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CartInfo {

    private String id;
    private List<ProductResponse> products;

    public CartInfo(ImmutableCart cart) {
        Objects.requireNonNull(cart);

        this.id = cart.getId();
        this.products = cart.getProducts().stream().map(ProductResponse::new).collect(Collectors.toList());

    }

    public String getId() {
        return id;
    }

    public List<ProductResponse> getProducts() {
        return products;
    }
}
