package org.krjura.wordpress.spring.restdocs.demo.model;

import java.util.Objects;

public class Product {

    private String id;

    private String group;

    private String name;

    private String nameForSearch;

    private String description;

    private float price;

    private String soldBy;

    private short rating;

    public Product(
            String id,
            String group,
            String name,
            String nameForSearch,
            String description,
            float price,
            String soldBy,
            short rating) {

        Objects.requireNonNull(id);
        Objects.requireNonNull(group);
        Objects.requireNonNull(name);
        Objects.requireNonNull(nameForSearch);
        Objects.requireNonNull(description);
        Objects.requireNonNull(soldBy);

        this.id = id;
        this.group = group;
        this.name = name;
        this.nameForSearch = nameForSearch;
        this.description = description;
        this.price = price;
        this.soldBy = soldBy;
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public String getGroup() {
        return group;
    }

    public String getName() {
        return name;
    }

    public String getNameForSearch() {
        return nameForSearch;
    }

    public String getDescription() {
        return description;
    }

    public float getPrice() {
        return price;
    }

    public String getSoldBy() {
        return soldBy;
    }

    public short getRating() {
        return rating;
    }
}