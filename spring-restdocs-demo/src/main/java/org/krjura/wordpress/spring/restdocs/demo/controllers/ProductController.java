package org.krjura.wordpress.spring.restdocs.demo.controllers;

import org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response.ProductResponses;
import org.krjura.wordpress.spring.restdocs.demo.model.Product;
import org.krjura.wordpress.spring.restdocs.demo.services.ProductService;
import org.krjura.wordpress.spring.restdocs.demo.utils.PageInfo;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Objects;

@Controller
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        Objects.requireNonNull(productService);

        this.productService = productService;
    }

    @GetMapping(
            path = "/api/v1/products",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<ProductResponses> listProducts(
            @RequestParam("group") String group,
            @RequestParam("searchTerm") String searchTerm,
            PageInfo pageInfo) {

        List<Product> products = this.productService.findProduct(group, searchTerm, pageInfo);

        return ResponseEntity.ok(new ProductResponses(products));
    }
}
