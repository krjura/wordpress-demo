package org.krjura.wordpress.spring.restdocs.demo.controllers;

import org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response.AddProductToCartRequest;
import org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response.CartInfo;
import org.krjura.wordpress.spring.restdocs.demo.pojo.ImmutableCart;
import org.krjura.wordpress.spring.restdocs.demo.services.CartService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Objects;
import java.util.Optional;

@Controller
public class CartController {

    private final CartService cartService;

    public CartController(CartService cartService) {
        Objects.requireNonNull(cartService);

        this.cartService = cartService;
    }

    @GetMapping(
            path = "/api/v1/carts",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<CartInfo> createCart() {
        ImmutableCart cart = this.cartService.createCart();

        return ResponseEntity.ok(new CartInfo(cart));
    }

    @GetMapping(
            path = "/api/v1/carts/{cartId}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<CartInfo> findCart(
            @PathVariable("cartId") String cartId) {

        Optional<ImmutableCart> cartOptional = this.cartService.findCart(cartId);

        if(cartOptional.isPresent()) {
            return ResponseEntity.ok(new CartInfo(cartOptional.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(
            path = "/api/v1/carts/{cartId}/products",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<CartInfo> addItemToCart(
            @PathVariable("cartId") String cartId,
            @RequestBody AddProductToCartRequest request) {

        ImmutableCart cart = this.cartService.addProductToCart(cartId, request.getGroup(), request.getProductId());

        return ResponseEntity.ok(new CartInfo(cart));
    }

    @DeleteMapping(
            path = "/api/v1/carts/{cartId}/products/{productId}"
    )
    public ResponseEntity<Void> removeProductFromCart(
            @PathVariable("cartId") String cartId,
            @PathVariable("productId") String productId) {

        boolean removed = this.cartService.removeProductFromCart(cartId, productId);

        return removed ? ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
    }
}
