package org.krjura.wordpress.spring.restdocs.demo.documentation.controllers;

import org.junit.Test;
import org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response.AddProductToCartRequest;
import org.krjura.wordpress.spring.restdocs.demo.controllers.pojo.response.CartInfo;
import org.krjura.wordpress.spring.restdocs.demo.documentation.DocumentationMvcTestBase;
import org.krjura.wordpress.spring.restdocs.demo.model.Product;
import org.krjura.wordpress.spring.restdocs.demo.pojo.ImmutableCart;
import org.krjura.wordpress.spring.restdocs.demo.services.CartService;
import org.krjura.wordpress.spring.restdocs.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.RequestDocumentation;

import java.util.UUID;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CartControllerDocumentation extends DocumentationMvcTestBase {

    @Autowired
    private ProductService productService;

    @Autowired
    private CartService cartService;

    @Test
    public void createACart() throws Exception {
        getMockMvc().perform(
                get("/api/v1/carts"))
                .andExpect(status().isOk())
                .andDo(document(
                        "create_cart",
                        PayloadDocumentation.responseFields(
                                DocumentationQueries.documentResponseIdOfTheCart(),
                                DocumentationQueries.documentResponseProductArray()
                        )
                ));
    }

    @Test
    public void findCart() throws Exception {
        String productId = UUID.randomUUID().toString();

        ImmutableCart cart = createCart();
        addDefaultProductToCart(productId, cart);

        getMockMvc().perform(
                get("/api/v1/carts/{cartId}", cart.getId()))
                .andExpect(status().isOk())
                .andDo(document(
                        "find_cart",
                        RequestDocumentation.pathParameters(
                                DocumentationQueries.documentQueryCartId()
                        ),
                        PayloadDocumentation.responseFields(
                                DocumentationQueries.documentResponseIdOfTheCart(),
                                DocumentationQueries.documentResponseProductArray(),
                                DocumentationQueries.documentResponseProductArrayId(),
                                DocumentationQueries.documentResponseProductArrayGroup(),
                                DocumentationQueries.documentResponseProductArrayName(),
                                DocumentationQueries.documentResponseProductArrayDescription(),
                                DocumentationQueries.documentResponseProductArrayPrice(),
                                DocumentationQueries.documentResponseProductArraySoldBy(),
                                DocumentationQueries.documentResponseProductArrayRating()
                        )
                ));
    }

    @Test
    public void addProductToCart() throws Exception {
        String productId = UUID.randomUUID().toString();

        ImmutableCart cart = createCart();
        addDefaultProductToCart(productId, cart);

        AddProductToCartRequest request = new AddProductToCartRequest()
                .productId(productId)
                .group("books");

        byte[] content = toJson(request);

        getMockMvc().perform(
                put("/api/v1/carts/{cartId}/products", cart.getId())
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(content))
                .andExpect(status().isOk())
                .andDo(document(
                        "add_product_to_cart",
                        RequestDocumentation.pathParameters(
                                DocumentationQueries.documentQueryCartId()
                        ),
                        PayloadDocumentation.requestFields(
                                DocumentationQueries.documentRequestGroup(),
                                DocumentationQueries.documentRequestProductId()
                        ),
                        PayloadDocumentation.responseFields(
                                DocumentationQueries.documentResponseIdOfTheCart(),
                                DocumentationQueries.documentResponseProductArray(),
                                DocumentationQueries.documentResponseProductArrayId(),
                                DocumentationQueries.documentResponseProductArrayGroup(),
                                DocumentationQueries.documentResponseProductArrayName(),
                                DocumentationQueries.documentResponseProductArrayDescription(),
                                DocumentationQueries.documentResponseProductArrayPrice(),
                                DocumentationQueries.documentResponseProductArraySoldBy(),
                                DocumentationQueries.documentResponseProductArrayRating()
                        )
                ));
    }

    @Test
    public void removeProductFromCart() throws Exception {
        String productId = UUID.randomUUID().toString();

        ImmutableCart cart = createCart();
        addDefaultProductToCart(productId, cart);

        getMockMvc().perform(
                delete("/api/v1/carts/{cartId}/products/{productId}", cart.getId(), productId))
                .andExpect(status().isNoContent())
                .andDo(document(
                        "remove_product_from_cart",
                        RequestDocumentation.pathParameters(
                                DocumentationQueries.documentQueryCartId(),
                                DocumentationQueries.documentQueryProductId()
                        )
                ));
    }

    private ImmutableCart createCart() {
        return cartService.createCart();
    }

    private void addDefaultProductToCart(String productId, ImmutableCart cart) {
        productService.addProduct("books", new Product(
                productId,
                "books",
                "Spring Explained",
                "spring explained",
                "Explains everything your want to know about spring",
                150.00f,
                "MySelf",
                (short) 5
        ));

        cartService.addProductToCart(cart.getId(), "books", productId);
    }
}
