package org.krjura.wordpress.spring.restdocs.demo.documentation.controllers;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.RequestDocumentation;

import java.util.List;

public class DocumentationQueries  {

    private DocumentationQueries() {

    }

    public static ParameterDescriptor DocumentQueryMaxItems() {
        return RequestDocumentation
                .parameterWithName("maxItems")
                .description("Maximum number of entries per page");
    }

    public static ParameterDescriptor DocumentQueryPage() {
        return RequestDocumentation
                .parameterWithName("page")
                .description("Page number");
    }

    public static ParameterDescriptor documentQuerySearchTerm() {
        return RequestDocumentation
                .parameterWithName("searchTerm")
                .description("partial name of a product to search for");
    }

    public static ParameterDescriptor documentQueryGroup() {
        return RequestDocumentation
                .parameterWithName("group")
                .description("Name of the group product belongs to");
    }

    public static FieldDescriptor documentResponseProductArrayRating() {
        return PayloadDocumentation
                .fieldWithPath("products[].rating")
                .type(JsonFieldType.NUMBER)
                .description("Average rating of the product");
    }

    public static FieldDescriptor documentResponseProductArraySoldBy() {
        return PayloadDocumentation
                .fieldWithPath("products[].soldBy")
                .type(JsonFieldType.STRING)
                .description("Name of the seller");
    }

    public static FieldDescriptor documentResponseProductArrayPrice() {
        return PayloadDocumentation
                .fieldWithPath("products[].price")
                .type(JsonFieldType.NUMBER)
                .description("Price of the product");
    }

    public static FieldDescriptor documentResponseProductArrayDescription() {
        return PayloadDocumentation
                .fieldWithPath("products[].description")
                .type(JsonFieldType.STRING)
                .description("Description of the product");
    }

    public static FieldDescriptor documentResponseProductArrayName() {
        return PayloadDocumentation
                .fieldWithPath("products[].name")
                .type(JsonFieldType.STRING)
                .description("Name of the product");
    }

    public static FieldDescriptor documentResponseProductArrayId() {
        return PayloadDocumentation
                .fieldWithPath("products[].id")
                .type(JsonFieldType.STRING)
                .description("Id of the product in the system");
    }

    public static FieldDescriptor documentResponseProductArrayGroup() {
        return PayloadDocumentation
                .fieldWithPath("products[].group")
                .type(JsonFieldType.STRING)
                .description("Name of the group product belong to");
    }

    public static FieldDescriptor documentResponseProductArray() {
        return PayloadDocumentation
                .fieldWithPath("products[]")
                .type(JsonFieldType.ARRAY)
                .description("Array containing search results");
    }

    public static FieldDescriptor documentResponseIdOfTheCart() {
        return PayloadDocumentation
                .fieldWithPath("id")
                .type(JsonFieldType.STRING)
                .description("Id of the cart in the system");
    }

    public static ParameterDescriptor documentQueryCartId() {
        return RequestDocumentation
                .parameterWithName("cartId")
                .description("Id of the cart in the system");
    }

    public static ParameterDescriptor documentQueryProductId() {
        return RequestDocumentation
                .parameterWithName("productId")
                .description("Id of the product in the system");
    }

    public static FieldDescriptor documentRequestGroup() {
        return PayloadDocumentation
                .fieldWithPath("group")
                .type(JsonFieldType.STRING)
                .description("Name of the group product belongs to");
    }

    public static FieldDescriptor documentRequestProductId() {
        return PayloadDocumentation
                .fieldWithPath("productId")
                .type(JsonFieldType.STRING)
                .description("Id of the product in the system");
    }
}
