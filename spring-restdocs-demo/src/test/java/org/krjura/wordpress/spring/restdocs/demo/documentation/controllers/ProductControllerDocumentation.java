package org.krjura.wordpress.spring.restdocs.demo.documentation.controllers;

import org.junit.Test;
import org.krjura.wordpress.spring.restdocs.demo.documentation.DocumentationMvcTestBase;
import org.krjura.wordpress.spring.restdocs.demo.model.Product;
import org.krjura.wordpress.spring.restdocs.demo.services.CartService;
import org.krjura.wordpress.spring.restdocs.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.RequestDocumentation;

import java.util.UUID;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerDocumentation extends DocumentationMvcTestBase {

    @Autowired
    private ProductService productService;

    @Autowired
    private CartService cartService;

    @Test
    public void listProductsSuccessfully() throws Exception {
        productService.addProduct("books", new Product(
                UUID.randomUUID().toString(),
                "books",
                "Spring Explained",
                "spring explained",
                "Explains everything your want to know about spring",
                150.00f,
                "MySelf",
                (short) 5
        ));
        productService.addProduct("books", new Product(
                UUID.randomUUID().toString(),
                "books",
                "Spring 5 In Detail",
                "spring 5 in detail",
                "Described changes in the newest version of spring",
                200.00f,
                "MySelf",
                (short) 5
        ));

        // execute
        getMockMvc().perform(
                get(
                        "/api/v1/products?group={group}&searchTerm={searchTerm}&page={page}&maxItems={maxItems}",
                        "books", "Spring", 0, 10))
                .andExpect(status().isOk())
                .andDo(document(
                        "list_products",
                        RequestDocumentation.requestParameters(
                                DocumentationQueries.documentQueryGroup(),
                                DocumentationQueries.documentQuerySearchTerm(),
                                DocumentationQueries.DocumentQueryPage(),
                                DocumentationQueries.DocumentQueryMaxItems()
                        ),
                        PayloadDocumentation.responseFields(
                                DocumentationQueries.documentResponseProductArray(),
                                DocumentationQueries.documentResponseProductArrayId(),
                                DocumentationQueries.documentResponseProductArrayGroup(),
                                DocumentationQueries.documentResponseProductArrayName(),
                                DocumentationQueries.documentResponseProductArrayDescription(),
                                DocumentationQueries.documentResponseProductArrayPrice(),
                                DocumentationQueries.documentResponseProductArraySoldBy(),
                                DocumentationQueries.documentResponseProductArrayRating()
                        )
                ));
    }
}
